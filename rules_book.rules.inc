<?php

/**
 * @file
 * rules_book.rules.inc
 * Custom actions for Rules.
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_book_rules_action_info() {
  return array(
    'rules_book_action_convert_node_to_book' => array(
      'label' => t('Convert node to book'),
      'group' => t('Book'),
      'parameter' => array(
        'rules_book_parameter_node' => array(
          'type' => 'node',
          'label' => t('Node to be converted'),
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_rules_condition_info().
 */
function rules_book_rules_condition_info() {
  return array(
    'rules_book_condition_type_is_allowed_book' => array(
      'label' => t('Node type is allowed to be a book'),
      'group' => t('Book'),
      'arguments' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node'),
        ),
      ),
      'module' => 'rules_book',
    ),
  );
}

/**
 * Callback for rules condition.
 *
 * Check node type is allowed to be a book.
 *
 * @param $node
 *   Node that should be converted to book.
 */
function rules_book_condition_type_is_allowed_book($node) {
  return _rules_book_type_is_allowed($node->type);
}

/**
 * Callback for rules action.
 *
 * Convert existing or new node to book.
 *
 * @param $node
 *   Node that should be converted to book.
 */
function rules_book_action_convert_node_to_book($node) {
  if (!is_object($node) || !_rules_book_type_is_allowed($node->type)) {
    drupal_set_message(t('Convert to book aborted. Node is not a book.'), 'error');
    return false;
  }
  $node->book['bid'] = $node->nid;
}

/**
 * Helper function to determine of the node type is allowed to be a book.
 *
 * @param $type
 *   String containing type of node.
 */
function _rules_book_type_is_allowed($type) {
  if (module_exists('book') && function_exists('book_type_is_allowed')) {
    return book_type_is_allowed($type);
  }
  return false;
}


